import os 
import sys 

import stat 

cur_dir = os.path.dirname(os.path.abspath(__file__))

exec_fn = os.path.join(cur_dir, 'drawwin.py')


while True:
    try:
        import drawwin
        import trans_lang
        import translate
    except ModuleNotFoundError as e:
        print('please:')
        print('\tpip3 install '+e.name)
        d = input('try again? [Y/n]: ')
        d = d.lower()
        if d == 'n' or d == 'no' :
            sys.exit(0)
        else:
            continue
    except Exception as e:
        print(e)
        sys.exit(0)
    break

# 给 可执行文件添加 python 解析器
with open(exec_fn, 'r') as f:
    lines = f.readlines()
with open(exec_fn, 'w') as n:
    n.write('#! '+ sys.executable+'\n')
    if lines[0][:2].strip() == '#!':
        lines = lines[1:]
    
    n.writelines(lines)

os.chmod(exec_fn, stat.S_IRWXU)



desktop  = '''[Desktop Entry]
Type=Application
Version=2.20210422.0
Name=tranlate
Comment=google tranlate
Exec=%s
Icon=%s
Categories=Development;IDE;
'''
desktop_fn = os.path.join(os.getenv('HOME'), '.local/share/applications/h_google_translate.desktop')

with open(desktop_fn,'w') as fw:
    fw.write(desktop%(exec_fn, os.path.join(cur_dir, 'trans.jpg')))

os.chmod(desktop_fn,stat.S_IRWXO|stat.S_IRWXU|stat.S_IRWXG)
