# ！/usr/bin/python3
# -*- coding: utf-8 -*-
import requests
import re
from trans_lang import trans_lang
 
def translated_content(text, target_language):
    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        # "accept-language": "en,zh-CN;q=0.9,zh;q=0.8",
        "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36"
    }
    # 请求url
    url = 'https://translate.google.cn/_/TranslateWebserverUi/data/batchexecute?f.sid=3624491441177242210&bl=boq_translate-webserver_20210419.13_p0&hl=zh-CN&soc-app=1&soc-platform=1&soc-device=1&rt=c'
    # 数据参数
    from_data = {
        "f.req": r"""[[["MkEWBc","[[\"{}\",\"auto\",\"{}\",true],[null]]",null,"generic"]]]""".format(text, target_language)
    }
    try:
        r = requests.post(url, headers=headers, data=from_data, timeout=10)
        # print(r.text)
        if r.status_code == 200:
            # 正则匹配结果
            # print(r.text)
            response = re.findall(r',\[\[\\"(.*?)\\",\[\\', r.text)
            if response:
                response = response[0]
            else:
                response = re.findall(r',\[\[\\"(.*?)\\"]', r.text)
                if response:
                    response = response[0]
            return response
    except Exception as e:
        print(e)
        return False
 

if __name__ == '__main__':
    # 翻译各个国家语言
    for i in trans_lang:
        response = translated_content("定制流程", i[1])
        print(i[0]+': '+response)

